import 'package:flutter_web/material.dart';
import 'package:silicon/silicon.dart';

class ServerTile extends StatelessWidget {
  final Server server;

  ServerTile({@required this.server});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(server.name),
      subtitle: Text('${server.hostname}:${server.port}'),
      onTap: () {},
    );
  }
}
