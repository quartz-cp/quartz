import 'package:flutter_web/material.dart';
import 'package:silicon/silicon.dart';
import 'package:quartz/widgets/server-tile.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Quartz CP',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final List<Server> servers = [
    Server(0, 'My server', 'localhost'),
    Server(1, 'Yinga server', 'yinga.peter.ovh'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Quartz CP'),
      ),
      body: ListView(
        children: servers.map((server) => ServerTile(server: server)).toList(),
      ),
    );
  }
}
