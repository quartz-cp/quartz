import 'package:silicon/silicon.dart';

/// Credit to #mcdevs, https://wiki.vg/Server_List_Ping#Response
class ServerStatus {
  ServerVersion version;
  ServerPlayers players;

  /// A [Chat] object.
  String description;

  /// A Base64 encoded PNG image.
  ///
  /// It does not contain newlines (since 1.13) and is prepended with
  /// "data:image/png;base64,". It is optional and not returned by all servers.
  String favicon;
}

class ServerVersion {
  final String name;
  final int protocol;

  ServerVersion(this.name, this.protocol);
}

class ServerPlayers {
  final int max;
  final int online;
  final List<Player> sample;

  ServerPlayers(this.max, this.online, this.sample);
}

enum OnlineStatus { ONLINE, OFFLINE, LOADING }
