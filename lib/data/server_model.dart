import 'package:quartz/data/server_status_model.dart';

class Server {
  final String name;
  final String host;
  final int port;

  OnlineStatus onlineStatus;
  int ping;
  ServerStatus serverStatus;

  Server(this.name, this.host, {this.port = 25565});
}
